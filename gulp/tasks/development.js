'use strict';

const config = require('../config'),
      gulp = require('gulp'),
      del = require('del'),
      sass = require('gulp-sass'),
      concat = require('gulp-concat'),
      watch = require('gulp-watch'),
      browserSync = require('browser-sync'),
      templateCache = require('gulp-angular-templatecache'),
      runSequence = require('run-sequence');

gulp.task('clean', function() {

  del([config.buildDir]);

});

// Views task
gulp.task('views', function() {

  // Put our index.html in the dist folder
  gulp.src(config.views.index)
    .pipe(gulp.dest(config.buildDir));

  // Process any other view files from app/views
  return gulp.src(config.views.src)
    .pipe(templateCache({ standalone: true }))
    .pipe(gulp.dest(config.views.dest))
    .pipe(browserSync.stream({ once: true }));

});

gulp.task('styles', function() {

  gulp.src(config.styles.src)
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('style.css'))
    .pipe(gulp.dest(config.styles.dest))
    .pipe(browserSync.stream({ once: true }));

});

gulp.task('images', function() {

  return gulp.src(config.images.src)
    .pipe(gulp.dest(config.images.dest))
    .pipe(browserSync.stream({ once: true }));

});

gulp.task('dev', ['clean'], function(cb) {

  global.isProd = false;

  runSequence(['styles', 'images', 'views', 'browserify'], 'watch', cb);

});
