'use strict';

const config = require('../config'),
      gulp = require('gulp'),
      gutil = require('gulp-util'),
      browserify = require('browserify'),
      watchify = require('watchify'),
      source = require('vinyl-source-stream'),
      browserSync = require('browser-sync'),
      handleErrors = require('../handleErrors')

function buildScript(file) {

  var bundler = browserify({
    entries: [config.sourceDir + 'js/' + file],
    extensions: ['.js'],
    debug: true,
    cache: {},
    packageCache: {},
    fullPaths: !global.isProd
  });

  if ( !global.isProd ) {
    bundler = watchify(bundler);

    bundler.on('update', function() {
      rebundle();
      gutil.log('Rebundle...');
    });
  }

  const transforms = [
    //{ 'name':babelify, 'options': {}},
  ];

  transforms.forEach(function(transform) {
    bundler.transform(transform.name, transform.options);
  });

  function rebundle() {
    const stream = bundler.bundle();

    return stream.on('error', handleErrors)
      .pipe(source(file))
      .pipe(gulp.dest(config.scripts.dest))
      .pipe(browserSync.stream({ once: true }));

  }

  return rebundle();
}

gulp.task('browserify', function() {

  return buildScript('main.js');

});
