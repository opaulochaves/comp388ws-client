'use strict';

function OnRun($rootScope, $state, $stateParams, AppSettings) {

  // change page title based on state
  $rootScope.$on('$stateChangeSuccess', (event, toState) => {
    $rootScope.pageTitle = '';

    if ( toState.title ) {
      $rootScope.pageTitle += toState.title;
      $rootScope.pageTitle += ' \u2015 ';
    }

    $rootScope.pageTitle += AppSettings.appTitle;
    $rootScope.$state = $state; // allow to $state from any scope in the app
    $rootScope.$stateParams = $stateParams;
  });

  $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {

    if (toState.requireLogin && typeof $rootScope.user === 'undefined') {
      event.preventDefault();
      $rootScope.nextState = toState.name;
      $state.go('login');
    }

  });

}

module.exports = [
  '$rootScope',
  '$state',
  '$stateParams',
  'AppSettings',
  OnRun
];
