'use strict';

function Customer($resource, AppSettings) {
  return {
    customer: $resource(AppSettings.baseUri + '/api/customers/:id', null, { 'update': { method: 'PUT' } }),
    orders: $resource(AppSettings.baseUri + '/api/customers/:id/orders')
  };
}

module.exports = {
  name: 'Customer',
  fn: ['$resource', 'AppSettings', Customer]
};
