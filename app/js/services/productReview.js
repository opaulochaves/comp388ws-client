'use strict';

function ProductReview($resource, AppSettings) {

  return $resource(AppSettings.baseUri + '/api/products/:productId/reviews/:id', {
    productId: '@productId',
    id: '@id'
  }, {
    'update': { method:'PUT' },
    'query': {
      method: 'GET',
      isArray: true,
      params: {
        productId: '@productId',
        id: '@id'
      }
    }
  });
}

module.exports = {
  name: 'ProductReview',
  fn: ['$resource', 'AppSettings', ProductReview]
};
