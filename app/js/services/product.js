'use strict';

function Product($resource, AppSettings) {

  return $resource(AppSettings.baseUri + '/api/products/:id', null,
  {
    'update': { method: 'PUT' }
  });
}

module.exports = {
  name: 'Product',
  fn: ['$resource', 'AppSettings', Product]
};
