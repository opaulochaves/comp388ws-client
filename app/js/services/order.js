'use strict';

function Order($resource, AppSettings) {

  return {
  	resource: $resource(AppSettings.baseUri + '/api/orders/:id', null,
	  {
	    'update': { method: 'PUT' }
	  }),

    payment: $resource(AppSettings.baseUri + '/api/orders/:id/payment'),

  	item: $resource(AppSettings.baseUri + '/api/orders/:id/items/:productId',
	  	{ id: '@id', productId: '@productId' },
	  	{ 'update': { method: 'PUT' } })
  };
}

module.exports = {
  name: 'Order',
  fn: ['$resource', 'AppSettings', Order]
};
