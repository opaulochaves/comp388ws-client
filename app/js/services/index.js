'use strict';

const servicesModule = angular.module('app.services', []);

const services = [
  require('./product'),
  require('./customer'),
  require('./seller'),
  require('./order'),
  require('./productReview'),
  require('./sellerReview'),
  require('./cart')
];

Object.keys(services).forEach((key) => {
  var item = services[key];

  servicesModule.service(item.name, item.fn);
});

module.exports = servicesModule;
