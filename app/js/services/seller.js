'use strict';

function Seller($resource, AppSettings) {

  return $resource(AppSettings.baseUri + '/api/sellers/:id', null,
  {
    'update': { method: 'PUT' }
  });
}

module.exports = {
  name: 'Seller',
  fn: ['$resource', 'AppSettings', Seller]
};
