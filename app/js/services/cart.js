'use strict';

function Cart($resource, AppSettings) {
  var cId;
  var resource = $resource(AppSettings.baseUri + '/api/customers/:customerId/cart/:id', {
    customerId: '@customerId',
    id: '@id'
  }, {
    'update': { method:'PUT' },
    'query': {
      method: 'GET',
      isArray: true,
      params: {
        customerId: '@customerId',
        id: '@id'
      }
    }
  });

  function getProducts() {
    if (!storageAvailable('localStorage'))
      return console.error('localStorage is not available!');
  }

  function addToCart(item) {
    if (!storageAvailable('localStorage'))
      return console.error('localStorage is not available!');

    var cart = localStorage.cart ? JSON.parse(localStorage.cart): null;

    if (!cart) {
      cart = [item];
    } else {
      var i = itemExists(item.product, cart);
      if (i !== false) {
        cart[i].quantity += item.quantity;
      } else {
        cart.push(item);
      }
    }
    localStorage.cart = JSON.stringify(cart);
    return cart;
  }

  function getItems() {
    if (!storageAvailable('localStorage'))
      return console.error('localStorage is not available!');

    return localStorage.cart ? JSON.parse(localStorage.cart): [];
  }

  function removeItem(productId) {
    if (!storageAvailable('localStorage'))
      return console.error('localStorage is not available!');

    var cart = localStorage.cart ? JSON.parse(localStorage.cart): [];
    cart = cart.filter(function(item) {
      return item.product !== productId;
    });
    localStorage.cart = JSON.stringify(cart);
    return cart;
  }

  function deleteCart() {
    if (!storageAvailable('localStorage'))
      return console.error('localStorage is not available!');

    localStorage.removeItem('cart');
    return !localStorage.getItem('cart');
  }

  // check if the item is already in the cart, if so, return its index
  function itemExists(id, cart) {
    for (var i = 0; i < cart.length; i++) {
      if (cart[i].product === id) {
        return i;
      }
    }
    return false;
  }

  return {
    getItems: getItems,
    addToCart: addToCart,
    deleteCart: deleteCart,
    removeItem: removeItem
  };
}

// https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API
function storageAvailable(type) {
	try {
		var storage = window[type],
			x = '__storage_test__';
		storage.setItem(x, x);
		storage.removeItem(x);
		return true;
	}
	catch(e) {
		return false;
	}
}

module.exports = {
  name: 'Cart',
  fn: ['$resource', 'AppSettings', Cart]
};
