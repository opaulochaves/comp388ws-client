'use strict';

function SellerReview($resource, AppSettings) {

  return $resource(AppSettings.baseUri + '/api/sellers/:sellerId/reviews/:id', {
    sellerId: '@sellerId',
    id: '@id'
  }, {
    'update': { method:'PUT' },
    'query': {
      method: 'GET',
      isArray: true,
      params: {
        sellerId: '@sellerId',
        id: '@id'
      }
    }
  });
}

module.exports = {
  name: 'SellerReview',
  fn: ['$resource', 'AppSettings', SellerReview]
};
