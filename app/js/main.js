'use strict';

var angular = require('angular');

require('angular-resource');
require('angular-ui-bootstrap');
require('angular-ui-router');
require('./templates');
require('./controllers');
require('./services');

const requires = [
  'ngResource',
  'ui.bootstrap',
  'ui.router',
  'templates',
  'app.controllers',
  'app.services'
];

// mount on window for testing
window.app = angular.module('app', requires);

angular.module('app').constant('AppSettings', require('./constants'));

angular.module('app').config(require('./on_config'));

angular.module('app').run(require('./on_run'));

angular.bootstrap(document, ['app'], {
  strictDi: true
});
