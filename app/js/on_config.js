'use strict';

function OnConfig($stateProvider, $locationProvider, $urlRouterProvider) {

  $locationProvider.html5Mode(true);

  $stateProvider
    .state('main', {
      url: '/',
      templateUrl: 'home.html',
      title: 'Home'
    })
    .state('login', {
      url: '/login',
      controller: 'LoginCtrl as login',
      templateUrl: 'login.html',
      title: 'Login'
    })
    .state('cart', {
      url: '/cart',
      controller: 'CartCtrl as cart',
      templateUrl: 'cart.html',
      title: 'cart'
    })
    .state('customers', {
      abstract: true,
      url: '/customers',
      controller: 'CustomersCtrl as customers',
      templateUrl: 'customers/index.html',
      title: 'Customers'
    })
      .state('customers.list', {
        url: '',
        templateUrl: 'customers/customers.list.html'
      })
      .state('customers.register', {
        url: '/register',
        templateUrl: 'customers/register.html'
      })
      .state('customers.detail', {
        url: '/{customerId:[0-9]{1,}}',
        templateUrl: 'customers/detail.html'
      })
      .state('customers.edit', {
        url: '/{customerId:[0-9]{1,}}/edit',
        templateUrl: 'customers/edit.html',
        controller: 'CustomersCtrl as customers',
      })

    .state('sellers', {
      abstract: true,
      url: '/sellers',
      controller: 'SellersCtrl as ctrl',
      templateUrl: 'sellers/index.html',
      title: 'Sellers'
    })
      .state('sellers.register', {
        url: '/register',
        templateUrl: 'sellers/register.html'
      })
      .state('sellers.detail', {
        url: '/{sellerId:[0-9]{1,}}',
        templateUrl: 'sellers/detail.html'
      })

    .state('products', {
      url: '/products',
      controller: 'ProductsCtrl as products',
      templateUrl: 'products/index.html',
      title: 'Products'
    })
      .state('products.detail', {
        url: '/{productId:[0-9]{1,}}',
        templateUrl: 'products/detail.html'
      })
      .state('products.form', {
        url: '/form',
        templateUrl: 'products/form.html'
      })

    .state('checkout', {
      url: '/checkout',
      controller: 'OrdersCtrl as ctrl',
      templateUrl: 'order/checkout.html',
      title: 'Checkout',
      requireLogin: true
    });

  $urlRouterProvider.otherwise('/');

}

module.exports = [
  '$stateProvider',
  '$locationProvider',
  '$urlRouterProvider',
  OnConfig
];
