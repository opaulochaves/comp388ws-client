'use strict';

const AppSettings = {
  appTitle: 'Example Application',
  apiUrl: '/api/v1',
  baseUri: 'http://localhost:8080',
  //baseUri: 'https://mysterious-inlet-4675.herokuapp.com'
};

module.exports = AppSettings;
