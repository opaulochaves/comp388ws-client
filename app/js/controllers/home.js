'use strict';

function HomeCtrl($scope, Product) {

  const vm = this;

  vm.title = 'AngularJS, Gulp, and Browserify!';
  vm.search = '';
  vm.products = [];
  vm.error;

  vm.submit = function() {
    vm.products = Product.query({q: vm.search});
  }

}

module.exports = {
  name: 'HomeCtrl',
  fn: ['$scope', 'Product', HomeCtrl]
};
