'use strict';

var app = require('angular');

const controllersModule = angular.module('app.controllers', []);

const controllers = [
  require('./main'),
  require('./home'),
  require('./login'),
  require('./customers'),
  require('./products'),
  require('./sellers'),
  require('./orders'),
  require('./cart')
];

Object.keys(controllers).forEach((key) => {
  var item = controllers[key];

  controllersModule.controller(item.name, item.fn);
});

module.exports = controllersModule;
