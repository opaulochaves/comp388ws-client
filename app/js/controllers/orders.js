'use strict';

function OrdersCtrl($rootScope, $scope, Order, Cart) {

  const vm = this;
  vm.error = undefined;
  vm.order = undefined;
  vm.paid = false;

  if ($rootScope.user && $rootScope.cart.length > 0) {
  	createOrder();
  }

  vm.payNow = function() {
    vm.paid = true;
    var payment = new Order.payment();
    payment.$save({ id: vm.order.id }, function (order) {
      vm.error = undefined;
      vm.order = order;
      Cart.deleteCart();
      $rootScope.cart = null;
    }, function (err) {
      vm.error = err;
    });
  };

  vm.deleteItem = function(productId) {
  	var item = new Order.item();
  	item.$remove({ id: vm.order.id, productId: productId }, function(order) {
  		vm.error = undefined;
  		vm.order = order;
  		$rootScope.cart = Cart.removeItem(productId);
  	}, function (err) {
  		vm.error = err;
  	});
  };

  function createOrder() {
  	var order = new Order.resource();
  	order.customer = $rootScope.user.user_id;
  	order.items = $rootScope.cart;
  	order.$save(function(order, headers) {
  		vm.error = undefined;
  		vm.order = order;
  	}, function(err) {
  		vm.error = err;
  	});
  }
}

module.exports = {
  name: 'OrdersCtrl',
  fn: ['$rootScope', '$scope', 'Order', 'Cart', OrdersCtrl]
};
