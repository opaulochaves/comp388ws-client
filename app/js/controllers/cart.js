'use strict';

function CartCtrl($rootScope, $state, Cart, Product) {

  const vm = this;
  vm.products = [];
  var cart = $rootScope.cart || [];
  var ids = '';

  if (cart.length > 0) {
    for (var i = 0; i < cart.length; i++) {
      ids += cart[i].product + ',';
    }

    vm.products = Product.query({q: vm.q, in: ids}, function() {
      // TODO doesn't smell good
      for (var i = 0; i < vm.products.length; i++) {
        var item = getItem(vm.products[i].id, cart);
        vm.products[i].cartQuantity = item.quantity;
        vm.products[i].cartIndex = item.pos;
      }
    });
  }

  vm.getTotal = function() {
    var total = 0;
    for (var i = 0; i < vm.products.length; i++) {
      total += vm.products[i].cartQuantity * vm.products[i].unit_price;
    }
    return total;
  }

  vm.deleteAll = function () {
    vm.products = [];
    Cart.deleteCart();
    $rootScope.cart = [];
  }

  vm.delete = function (pId) {
    vm.products = vm.products.filter(function(product) {
      return pId !== product.id;
    });
    Cart.removeItem(pId);
    $rootScope.cart = Cart.getItems();
  }
}

function getItem(pId, items) {
  for (var i = 0; i < items.length; i++) {
    if (items[i].product === pId) {
      return {
        'pos': i,
        'quantity': items[i].quantity
      };
    }
  }
}

module.exports = {
  name: 'CartCtrl',
  fn: ['$rootScope', '$state', 'Cart', 'Product', CartCtrl]
};
