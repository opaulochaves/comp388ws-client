'use strict';

function ProductsCtrl($rootScope, $scope, $state, Product, ProductReview) {

  const vm = this;

  vm.search = '';
  vm.products = [];
  vm.product;
  vm.quantity = 1; // used on products.detail
  vm.error = null;

  getProduct();

  vm.addToCart = function addToCart() {
    $scope.$parent.main.addToCart(vm.quantity, vm.product);
  };

  vm.save = function(form) {
    if (!form) {
      console.error("Form is undefined");
      return;
    }
    if (form.$invalid) {
      return;
    };

    var product = new Product(vm.product);
    product.$save(
      function (product, headers) {
        vm.error = null;
        vm.product = product;
        $state.go('products.detail', { productId: product.id });
      },
      function (err) {
        if (err) {
          if (err.status === 500) { // error comes in a single object
            vm.error.push({ message: err.data.message })
          } else { // error comes as an array
            err.data.forEach(function (data) {
              vm.error.push({ message: data.message });
            });
          }
        }
      }
    );
  };

  vm.saveReview = function(review) {
    review.customer = $rootScope.user.user_id;
    review.product = vm.product.id;
    var reviewProduct = new ProductReview(review);
    reviewProduct.$save({productId: vm.product.id}, function (review, headers) {
      vm.reviews.push(review);
    }, function(err) {
      vm.error = err.data.message;
    });
  };

  function getProduct() {
    var product;
    var id = parseInt($state.params.productId) || null;
    var products = $scope.$parent.main.products || null;

    if (id) {
      if (products && products.length > 0) {
        findById(id, products);
      } else {
        product = Product.get({ id: id }, function (product) {
          vm.product = product;
          getReviews();
        });
      }
    }
    return product;
  }

  function getReviews() {
    if (vm.product) {
      vm.reviews = ProductReview.query({ productId: vm.product.id }, function(reviews) {
        vm.reviews = reviews;
      });
    }
  }

  function findById(id, list) {
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    var result = list.filter(function(obj) {
      if ('id' in obj && obj.id === id) {
        return true;
      }
    });
    if (result) {
      vm.product = result[0];
      getReviews();
    }
  }
}

module.exports = {
  name: 'ProductsCtrl',
  fn: ['$rootScope', '$scope', '$state', 'Product', 'ProductReview', ProductsCtrl]
};
