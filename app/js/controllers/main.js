'use strict';

function MainCtrl($rootScope, $state, Product, Cart) {

  const vm = this;

  vm.q = '';
  vm.products = [];
  vm.error;
  $rootScope.user;
  $rootScope.cart = Cart.getItems();

  vm.search = function search() {
    vm.products = Product.query({q: vm.q});
  }

  vm.addToCart = function addToCart(quantity, product) {
    $rootScope.cart = Cart.addToCart({
      product: product.id,
      quantity: quantity
    });
  }
}

function itemExists(items, id) {
  for (var i = 0; i < items.length; i++) {
    if (items[i].product === id) {
      return i;
    }
  }
  return;
}

module.exports = {
  name: 'MainCtrl',
  fn: ['$rootScope', '$state', 'Product', 'Cart', MainCtrl]
};
