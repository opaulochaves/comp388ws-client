'use strict';

function CustomersCtrl($scope, $state, Customer) {

  var vm = this;
  vm.customer = undefined;
  vm.error = [];

  if ($state.params.customerId) {
    if (!vm.customer) { // if there's no customer
      var id = $state.params.customerId;
      vm.customer = Customer.customer.get({ id: id }, function (customer) {

        vm.customer = customer;
        vm.orders = Customer.orders.query({ id: customer.id });
      });
    }
  }

  vm.save = function save(form) {
    if (!form) {
      console.error("Form is undefined");
      return;
    }
    if (form.$invalid) {
      return;
    }

    var customer = new Customer.customer(vm.customer);
    customer.$save(
      function (customer, headers) {
        vm.error = [];
        vm.customer = customer;
        $state.go('customers.detail', { customerId: customer.id });
      },
      function (err) {
        if (err) {
          if (err.status === 500) { // error comes in a single object
            vm.error.push({ message: err.data.message });
          } else { // error comes as an array
            err.data.forEach(function (data) {
              vm.error.push({ message: data.message });
            });
          }
        }
      }
    );
  };

  vm.update = function() {
    console.log($scope.form);
    if ($scope.form.$invalid) {
      return;
    }

  }

  vm.orderTotal = function(order) {
    var total = 0;
    for (var item of order.items) {
      total += item.quantity * item.unit_price;
    }
    return total;
  };

}

module.exports = {
  name: 'CustomersCtrl',
  fn: ['$scope', '$state', 'Customer', CustomersCtrl]
};
