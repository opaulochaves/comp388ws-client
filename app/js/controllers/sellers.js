'use strict';

function SellersCtrl($scope, $state, Seller, SellerReview) {

  const vm = this;
  vm.seller;
  vm.reviews;
  vm.error = [];
  var sellerId = $state.params.sellerId || null;

  if (sellerId > 0) { // TODO make this generic and use for different ctrs
    getSeller(sellerId);
  }

  vm.save = function(form) {
    if (!form) {
      console.error("Form is undefined");
      return;
    }
    if (form.$invalid) {
      return;
    }
    var seller = new Seller(vm.seller);
    seller.type = 'Personal';
    seller.$save(
      function (seller, headers) {
        vm.error = [];
        vm.seller = seller;
        $state.go('sellers.detail', { sellerId: seller.id });
      },
      function (err) {
        if (err) {
          if (Array.isArray(err.data)) {
            err.data.forEach(function (data) {
              vm.error.push({ message: data.message });
            });
          } else {
            vm.error.push({ message: err.data })
          }
        }
      }
    );
  }

  function getSeller(id) {
    Seller.get({ id: id }, function(seller) {
      vm.seller = seller;
      getReviews(id);
    });
  }

  function getReviews(id) {
    if (vm.seller) {
      SellerReview.query({ sellerId: id }, function(reviews) {
        vm.reviews = reviews;
      });
    }
  }

}

module.exports = {
  name: 'SellersCtrl',
  fn: ['$scope', '$state', 'Seller', 'SellerReview', SellersCtrl]
};
