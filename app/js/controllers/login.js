'use strict';

function LoginCtrl($rootScope, $scope, $state, $resource, AppSettings) {

  var vm = this;
  vm.user = {};
  vm.error;

  vm.login = function (user) {
  	if ($scope.form.$invalid) {
      return;
    }

  	vm.user = angular.copy(user);
  	var resource = $resource(AppSettings.baseUri + '/api/login', null, { 'login': { method: 'POST' } });
    resource.login(user, function(user) {
    	vm.error = undefined;
      if (user.type === 'customer') {
        $rootScope.user = user;
        $state.go($rootScope.nextState || 'main');
      } else {
        vm.error = 'No customer found with this email';
      }
    }, function (err) {
    	if (err) {
	    	if (err.status == 401) {
	    		vm.error = err.data.message;
	    	}
    	}
    });
  }

}

module.exports = {
  name: 'LoginCtrl',
  fn: ['$rootScope', '$scope', '$state', '$resource', 'AppSettings', LoginCtrl]
};
