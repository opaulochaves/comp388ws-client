## COMP 388 - Web Services - Final Project Client

- Install gulp globally
```sh
$ npm install -g gulp gulp-cli
```

- Install all packages
```sh
$ npm install
```

- Run in development mod
```sh
$ gulp dev
```

-------

#### Reference
- https://github.com/jakemmarsh/angularjs-gulp-browserify-boilerplate
